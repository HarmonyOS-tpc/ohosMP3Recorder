package com.czt.mp3recorder;

public interface IDataEncode {
    void sendStopMessage();

    void addTask(short[] rawData, int readSize);
    void start();
}
