package com.czt.mp3recorder;

import com.czt.mp3recorder.util.LameUtil;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.audio.AudioCapturer;
import ohos.media.audio.FrameIntervalObserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class DataEncodeThread extends Thread implements FrameIntervalObserver, IDataEncode {
    private final String TAG = DataEncodeThread.class.getSimpleName();
    private ProcessHandler mHandler;
    private static final int PROCESS_STOP = 1;
    private static final int PROCESS_ING = 2;
    private byte[] mMp3Buffer;
    private FileOutputStream mFileOutputStream;
    private EventRunner mEventRunner;
    private AudioCapturer mAudioCapture;
    private List<Task> mTasks = Collections.synchronizedList(new ArrayList<Task>());
    private Timer mTimer ;
    private TimerTask mTimerTask = new TimerTask() {
        @Override
        public void run() {
            onFrameInterval();
        }
    };


    private static class ProcessHandler extends EventHandler {
        private DataEncodeThread encodeThread;
        private ProcessHandler(EventRunner looper, DataEncodeThread encodeThread) {
            super(looper);
            this.encodeThread = encodeThread;
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event.eventId == PROCESS_STOP) {
                //处理缓冲区中的数据
                while (encodeThread.processData() > 0) ;
                // Cancel any event left in the queue
                removeAllEvent();
                encodeThread.flushAndRelease();
                encodeThread.mEventRunner.stop();
                encodeThread.mEventRunner = null;
                encodeThread = null;
            }else if(event.eventId == PROCESS_ING) {
               while (encodeThread.processData()>0);
            }
        }
    }

    @Override
    public void onFrameInterval() {
        mHandler.sendEvent(PROCESS_ING);
    }

    /**
     * Constructor
     *
     * @param file       file
     * @param bufferSize bufferSize
     * @throws FileNotFoundException file not found
     */
    public DataEncodeThread(File file, int bufferSize, AudioCapturer audioCapturer) throws FileNotFoundException {
        this.mFileOutputStream = new FileOutputStream(file);
        mMp3Buffer = new byte[(int) (7200 + (bufferSize * 2 * 1.25))];
        mAudioCapture = audioCapturer;
    }

    @Override
    public void run() {
        super.run();
        mEventRunner = EventRunner.create(false);
        mHandler = new ProcessHandler(mEventRunner, this);
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(mTimerTask,0,160);
        mEventRunner.run();
    }

    private void check() {
        if (mHandler == null) {
            throw new IllegalStateException();
        }
    }

    @Override
    public void sendStopMessage() {
        check();
        mHandler.sendEvent(PROCESS_STOP);
    }

    public EventHandler getHandler() {
        check();
        return mHandler;
    }

    /**
     * 从缓冲区中读取并处理数据，使用lame编码MP3
     *
     * @return 从缓冲区中读取的数据的长度
     * 缓冲区中没有数据时返回0
     */
    private int processData() {
        if (mTasks.size() > 0) {
            Task task = mTasks.remove(0);
            short[] buffer = task.getData();
            int readSize = task.getReadSize();
            int encodedSize = LameUtil.encode(buffer, buffer, readSize, mMp3Buffer);
            if (encodedSize > 0) {
                try {
                    mFileOutputStream.write(mMp3Buffer, 0, encodedSize);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return readSize;
        }
        return 0;
    }

    /**
     * Flush all data left in lame buffer to file
     */
    private void flushAndRelease() {
        //将MP3结尾信息写入buffer中
        mTimerTask.cancel();
        mTimer.cancel();
        final int flushResult = LameUtil.flush(mMp3Buffer);
        if (flushResult > 0) {
            try {
                mFileOutputStream.write(mMp3Buffer, 0, flushResult);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (mFileOutputStream != null) {
                    try {
                        mFileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                LameUtil.close();
            }
        }
    }
    @Override
    public void addTask(short[] rawData, int readSize) {
        mTasks.add(new Task(rawData, readSize));
    }
    private class Task {
        private short[] rawData;
        private int readSize;
        public Task(short[] rawData, int readSize) {
            this.rawData = rawData.clone();
            this.readSize = readSize;
        }
        public short[] getData() {
            return rawData;
        }
        public int getReadSize() {
            return readSize;
        }
    }
}
