package com.czt.mp3recorder;


import ohos.media.audio.AudioStreamInfo;

public enum PCMFormat {
	PCM_8BIT (1, AudioStreamInfo.EncodingFormat.ENCODING_PCM_8BIT),
	PCM_16BIT (2, AudioStreamInfo.EncodingFormat.ENCODING_PCM_16BIT);
	
	private int bytesPerFrame;
	private AudioStreamInfo.EncodingFormat audioFormat;
	
	PCMFormat(int bytesPerFrame, AudioStreamInfo.EncodingFormat audioFormat) {
		this.bytesPerFrame = bytesPerFrame;
		this.audioFormat = audioFormat;
	}
	public int getBytesPerFrame() {
		return bytesPerFrame;
	}
	public AudioStreamInfo.EncodingFormat getAudioFormat() {
		return audioFormat;
	}
}
