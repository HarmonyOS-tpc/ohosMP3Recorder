## 使用方法
`implementation 'io.openharmony.tpc.thirdlib:mp3recorderlib:1.0.0'`

编译(IDE Build->build haps)之后，复制 mp3reocrderlib/build/intermediates/cmake/debug/obj/arm64-v8a到库文件中

## 录音功能实现
1. 创建MP3Recorder， 传入录音文件的File对象。
	```java
	MP3Recorder mRecorder = new MP3Recorder(new File(Environment.getExternalStorageDirectory(),"test.mp3"));
	```
2. 开始录音： 调用MP3Recorder对象的start()方法。
	```java
	mRecorder.start();
	```
3. 停止录音： 调用MP3Recorder对象的stop()方法。
	```java
	mRecorder.stop();
	```

代码示例见： MainAbilitySlice
#  关于音量部分的解释
音量的计算，来自于 [三星开发者文档-Displaying Sound Volume in Real-Time While Recording](http://developer.samsung.com/technical-doc/view.do?v=T000000086)    
里面对于音量的最大值设置为了4000，而我实际测验中发现大部分声音不超过2000，所以就在代码中暂时设置为2000。  
这方面没有找到相关资料，如果有人知道理论值之类的，请联系我(chentong.think@gmail.com) 完善此库，谢谢。