package com.czt.mp3recorder.sample.slice;

import com.czt.mp3recorder.MP3Recorder;
import com.czt.mp3recorder.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.common.Source;
import ohos.media.player.Player;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    private final String TAG = MainAbilitySlice.class.getSimpleName();

    private File file = null;
    private MP3Recorder mRecorder;
    private Button mStartStopBtn;
    private Button mPlayBtn;
    private boolean mStarted = false;
    private Player mPlayer;
    private EventHandler mHander;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initRecorder();
        mHander = new EventHandler(EventRunner.getMainEventRunner());
        mPlayer = new Player(this);
        mStartStopBtn = (Button) findComponentById(ResourceTable.Id_start);
        mPlayBtn = (Button) findComponentById(ResourceTable.Id_play);
        mStartStopBtn.setClickedListener((view) -> {
            mStartStopBtn.setClickable(false);
            new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> mStartStopBtn.setClickable(true), 500);
            if (!mStarted) {
                try {
                    mStarted = true;
                    mRecorder.start();
                    mStartStopBtn.setText(ResourceTable.String_stop_record);
                } catch (IOException e) {
                    mStarted = false;
                    mStartStopBtn.setText(ResourceTable.String_start_record);
                    e.printStackTrace();
                }
            } else {
                mStarted = false;
                mRecorder.stop();
                mStartStopBtn.setText(ResourceTable.String_start_record);
            }
        });

        mPlayBtn.setClickedListener((view) -> {
            if (mPlayer.isNowPlaying()) {
                mPlayer.stop();
                mPlayBtn.setText(ResourceTable.String_start_play);
            } else {
                startPlay();
                mPlayBtn.setText(ResourceTable.String_stop_play);
            }
        });
    }

    private void startPlay() {
        if (file.exists()) {
            mPlayer.reset();
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file.getCanonicalFile());
                mPlayer.setSource(new Source(inputStream.getFD()));
                mPlayer.prepare();
                mPlayer.setPlayerCallback(new Player.IPlayerCallback() {
                    @Override
                    public void onPrepared() {
                    }

                    @Override
                    public void onMessage(int i, int i1) {
                    }

                    @Override
                    public void onError(int i, int i1) {
                    }

                    @Override
                    public void onResolutionChanged(int i, int i1) {
                    }

                    @Override
                    public void onPlayBackComplete() {
                        mHander.postTask(() -> mPlayBtn.setText(ResourceTable.String_start_play));
                    }

                    @Override
                    public void onRewindToComplete() {
                        mHander.postTask(() -> mPlayBtn.setText(ResourceTable.String_start_play));
                    }

                    @Override
                    public void onBufferingChange(int i) {
                    }

                    @Override
                    public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {
                    }

                    @Override
                    public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {
                    }
                });
                mPlayer.play();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void initRecorder() {
        file = new File(getFilesDir(), "record.mp3");
        try {
             file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mRecorder = new MP3Recorder(file);
    }

}
