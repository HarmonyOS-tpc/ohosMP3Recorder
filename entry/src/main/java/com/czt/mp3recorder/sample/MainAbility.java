package com.czt.mp3recorder.sample;

import com.czt.mp3recorder.sample.slice.MainAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {
    private int mPermissionReqCode = 100;
    private String[] mNeedPermissions = {"ohos.permission.MICROPHONE"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        if (verifyCallingPermission(mNeedPermissions[0]) != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(mNeedPermissions, mPermissionReqCode);
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == mPermissionReqCode) {
            if (grantResults[0] != IBundleManager.PERMISSION_GRANTED) {
                terminateAbility();
            }
        } else {
            terminateAbility();
        }
    }
}
